#include "C:\Users\33695\Documents\git\pic_c_port_coulissant\main.h"
#define motor0L output_low(pin_d0)
#define motor0H output_high(pin_d0)
#define motor1L output_low(pin_d1)
#define motor1H output_high(pin_d1)
#define rfid_int input(pin_b0)
#define rfid_ext input(pin_b1)
#define cPO input(pin_b6)
#define cPF input(pin_b7)
#define BC input(pin_b2)
#define BI input(pin_b3)
#define open_BC_duration 2
#define open_RFID_duration 2
void motorStop()
{
   motor0L;
   motor1L;
}

void motorOpen()
{
   motor0H;
   motor1L;
}

void motorClose()
{
   motor0L;
   motor1H;
}

void delaySec(int delaySecTime)
{
   int index=0;
   while(index<delaySecTime)
   {
   delay_ms(1000);
   index++;
   }
}

void open(){
while(cPO==1){motorOpen();}
motorStop();
}

void close()
{
   
   while(BI && cPO){open();}
   while(!BI && cPF){motorClose();}
   
motorStop();
}

void main()
{   
   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
//Setup_Oscillator parameter not selected from Intr Oscillator Config tab
   motorStop();
   // TODO: USER CODE!!
   while(1)
   {
      if(BC)
      {
      open();
      delaySec(open_BC_duration);
      close();
      }
      
      if(rfid_int || rfid_ext)
      {
      open();
      delaySec(open_RFID_duration);
      close();
      }
   }

}



